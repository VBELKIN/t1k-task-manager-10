package ru.t1k.vbelkin.tm.model;

import java.util.UUID;

public final class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Project() {
    }

    public Project(String name) {
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
