package ru.t1k.vbelkin.tm;

import ru.t1k.vbelkin.tm.api.ICommandController;
import ru.t1k.vbelkin.tm.api.ICommandRepository;
import ru.t1k.vbelkin.tm.api.ICommandService;
import ru.t1k.vbelkin.tm.component.Bootstrap;
import ru.t1k.vbelkin.tm.constant.ArgumentConst;
import ru.t1k.vbelkin.tm.constant.TerminalConst;
import ru.t1k.vbelkin.tm.controller.CommandController;
import ru.t1k.vbelkin.tm.model.Command;
import ru.t1k.vbelkin.tm.repository.CommandRepository;
import ru.t1k.vbelkin.tm.service.CommandService;
import ru.t1k.vbelkin.tm.util.FormatUtil;

import java.util.Scanner;

import static ru.t1k.vbelkin.tm.constant.TerminalConst.*;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}

