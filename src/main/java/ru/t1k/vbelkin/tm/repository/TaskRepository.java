package ru.t1k.vbelkin.tm.repository;

import ru.t1k.vbelkin.tm.api.ITaskRepository;
import ru.t1k.vbelkin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task){
        tasks.add(task);
        return task;
    }

    @Override
    public void clear(){
        tasks.clear();
    }

    @Override
    public List<Task> findAll(){
        return tasks;
    }

}
