package ru.t1k.vbelkin.tm.repository;

import ru.t1k.vbelkin.tm.api.IProjectRepository;
import ru.t1k.vbelkin.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project add(final Project project){
        projects.add(project);
        return project;
    }

    @Override
    public void clear(){
        projects.clear();
    }

    @Override
    public List<Project> findAll(){
        return projects;
    }

}
