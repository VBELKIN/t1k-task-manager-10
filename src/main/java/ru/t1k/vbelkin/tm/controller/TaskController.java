package ru.t1k.vbelkin.tm.controller;

import ru.t1k.vbelkin.tm.api.ITaskController;
import ru.t1k.vbelkin.tm.api.ITaskService;
import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.model.Task;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task : tasks)
            System.out.println(index + ". " + task);
        index++;
        System.out.println("[OK]");
    }

}
