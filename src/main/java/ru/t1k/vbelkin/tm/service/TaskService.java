package ru.t1k.vbelkin.tm.service;

import ru.t1k.vbelkin.tm.api.ITaskRepository;
import ru.t1k.vbelkin.tm.api.ITaskService;
import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(Task task) {
        if (task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }
}
