package ru.t1k.vbelkin.tm.service;

import ru.t1k.vbelkin.tm.api.IProjectRepository;
import ru.t1k.vbelkin.tm.api.IProjectService;
import ru.t1k.vbelkin.tm.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name, final String description){
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return add(new Project(name, description));
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

}
