package ru.t1k.vbelkin.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
