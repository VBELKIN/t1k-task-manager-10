package ru.t1k.vbelkin.tm.api;

import ru.t1k.vbelkin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name, String description);

    void clear();

    List<Task> findAll();

}
