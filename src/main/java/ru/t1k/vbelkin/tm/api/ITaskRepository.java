package ru.t1k.vbelkin.tm.api;

import ru.t1k.vbelkin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

}
