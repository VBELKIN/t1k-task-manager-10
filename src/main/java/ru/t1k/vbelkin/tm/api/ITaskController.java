package ru.t1k.vbelkin.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
