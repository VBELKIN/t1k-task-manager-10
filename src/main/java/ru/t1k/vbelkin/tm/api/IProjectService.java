package ru.t1k.vbelkin.tm.api;

import ru.t1k.vbelkin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create (String name, String description);

    void clear();

    List<Project> findAll();

}
