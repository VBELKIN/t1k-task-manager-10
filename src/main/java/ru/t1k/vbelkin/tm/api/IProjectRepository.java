package ru.t1k.vbelkin.tm.api;

import ru.t1k.vbelkin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

}
